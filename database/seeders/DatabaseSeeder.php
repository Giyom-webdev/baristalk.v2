<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(UsersTableSeeder::class);
         $this->call(FriendshipTableSeeder::class);
         $this->call(PostTableSeeder::class);
         /* \App\Models\Food_user::factory(3)->create(); */
        //\App\Models\Food::factory(9)->create();
    }
}
