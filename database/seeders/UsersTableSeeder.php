<?php

namespace Database\Seeders;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        {
            DB::table('users')->insert([
                'username' => 'User1',
                'email' => 'user1@gmail.com',
                'password' => bcrypt('admin'),
                'city' => 'Montreal',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
            DB::table('users')->insert([
                'username' => 'User2',
                'email' => 'user2@gmail.com',
                'password' => bcrypt('admin'),
                'city' => 'Montreal',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
            DB::table('users')->insert([
                'username' => 'User3',
                'email' => 'user3@gmail.com',
                'password' => bcrypt('admin'),
                'city' => 'Montreal',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
            DB::table('users')->insert([
                'username' => 'User4',
                'email' => 'user4@gmail.com',
                'password' => bcrypt('admin'),
                'city' => 'Montreal',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
 
        }
    }
}
