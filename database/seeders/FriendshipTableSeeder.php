<?php

namespace Database\Seeders;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class FriendshipTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('friendships')->insert([
            'sent' => '1',
            'received' => '2',
            'accepted' => '1',
            'created_at' => '2021-12-03 19:53:47',
            'updated_at' => '2021-12-03 19:53:47',
        ]);
        DB::table('friendships')->insert([
            'sent' => '3',
            'received' => '1',
            'accepted' => '1',
            'created_at' => '2021-12-03 19:53:47',
            'updated_at' => '2021-12-03 19:53:47',
        ]);
        DB::table('friendships')->insert([
            'sent' => '1',
            'received' => '4',
            'accepted' => '1',
            'created_at' => '2021-12-03 19:53:47',
            'updated_at' => '2021-12-03 19:53:47',
        ]);
        DB::table('friendships')->insert([
            'sent' => '2',
            'received' => '3',
            'accepted' => '1',
            'created_at' => '2021-12-03 19:53:47',
            'updated_at' => '2021-12-03 19:53:47',
        ]);
        DB::table('friendships')->insert([
            'sent' => '3',
            'received' => '4',
            'accepted' => '1',
            'created_at' => '2021-12-03 19:53:47',
            'updated_at' => '2021-12-03 19:53:47',
        ]);
        DB::table('friendships')->insert([
            'sent' => '4',
            'received' => '2',
            'accepted' => '1',
            'created_at' => '2021-12-03 19:53:47',
            'updated_at' => '2021-12-03 19:53:47',
        ]);
    }
}
