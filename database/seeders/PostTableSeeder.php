<?php

namespace Database\Seeders;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PostTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('posts')->insert([
            'username' => 'Nabil Bouazzaoui',
            'user_id' => '1',
            'description' => 'Lorem Ipsum Dolor Sit Amet',
            'created_at' => '2021-10-03 16:22:15',
            'updated_at' => '2021-10-03 16:22:15',
        ]);
        DB::table('posts')->insert([
            'username' => 'Guillaume Gosselin-B.',
            'user_id' => '2',
            'description' => 'Lorem Ipsum Dolor Sit Amet',
            'created_at' => '2021-12-03 18:22:15',
            'updated_at' => '2021-12-03 18:22:15',
        ]);
        DB::table('posts')->insert([
            'username' => 'David Morel',
            'user_id' => '3',
            'description' => 'Lorem Ipsum Dolor Sit Amet',
            'created_at' => '2021-12-03 22:22:15',
            'updated_at' => '2021-12-03 22:22:15',
        ]);
        DB::table('posts')->insert([
            'username' => 'Emeline Leclere',
            'user_id' => '4',
            'description' => 'Lorem Ipsum Dolor Sit Amet',
            'created_at' => '2021-12-03 20:22:15',
            'updated_at' => '2021-12-03 20:22:15',
        ]);
    }
}
