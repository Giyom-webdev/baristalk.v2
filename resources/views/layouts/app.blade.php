<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />


    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <!-- FavIcon -->
    <link rel="icon" href="{{ url('css/favicon.ico') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('css/style.css') }}">


    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script type="text/javascript" src="{{ asset('js/form_animation.js') }}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.2/bootstrap3-typeahead.min.js"></script>



</head>



<body class="bg-beige w-screen text-bleu font-poppins mt-3">

    @include('layouts.navigation')
    <div id="gradient_body" class="min-h-screen">
        <!-- Page Heading -->
        <header class="text-bleu mt-16">
            <div class="mx-auto  sm:px-6 lg:px-8">
                {{ $header }}
            </div>
        </header>

        <!-- Page Content -->
        <main>
            {{ $slot }}
        </main>
    </div>
    <!-- <script>
        if (document.readyState !== 'loading') {
            document.querySelector("body").style.visibility = "visible";
                document.querySelector("#loader").style.visibility = "hidden";
        } else {
            document.addEventListener('DOMContentLoaded', function() {

                document.querySelector("body").style.visibility = "hidden";
            document.querySelector("#loader").style.visibility = "visible";
            })};

    </script> -->
</body>

</html>
