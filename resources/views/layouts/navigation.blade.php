<nav x-data="{ open: false }" class="px-6 mt-8">

    <!-- Primary Navigation Menu -->
    <div class="flex justify-between ">
        <!-- Navigation Links -->
        @auth
        <a href="{{ route('dashboard') }}"><x-logo class="h-20 p-2 ml-12 w-24"></x-logo></a>
        <a href="{{ route('dashboard') }}" class="text-base hover:text-bleu font-medium hover:font-semibold mt-4 hover:no-underline tracking-widest">BARISTALK</a>

        @endauth
        <!-- Les demandes d'amitiés -->
        <div class="flex">
        <div class="flex place-items-center">
        <x-dropdown class="align-right" width="60">
            <x-slot name="trigger">
                <button class="mr-8">
                    @auth
                    <div class="ml-4 text-rose">{{$notifications}}</div>
                    @else
                    <div>Connexion</div>
                    @endauth
                    <div class="ml-1">
                        <i class="fas fa-user-alt text-xl dropdown_svg"></i>
                    </div>
                </button>
            </x-slot>

            <x-slot name="content">
                <!-- Authentication -->

                @if (isset($friendRequest) && $friendRequest -> isNotEmpty())
                <h1 class="text-center">DEMANDE D'AMIS</h1>
                @foreach($friendRequest as $request)
                <div class="flex mb-4">

                    <hr>
                    <div class="text-rose p-4 uppercase">{{$request->username}}</div>
                    <form method="POST" action="{{ route('accept.friend') }}" class="flex">
                    @csrf
                    {{ method_field('PUT') }}

                    <button id="accepted" type="submit" name="accepted" value="{{$request->id}}" class="ml-4 p-2">
                    <i class="far fa-check-circle text-green-600 text-xl"></i>
                    </button>
                    </form>
                    <form method="POST" action="{{route ('refuse.friend') }}" class="flex">
                    @csrf
                    {{ method_field('POST') }}
                    <button id="refuse" type="submit" name="refuse" value="{{$request->id}}" class="p-2">
                    <i class="far fa-times-circle text-red-600 text-xl"></i>
                    </button>

                    </div>
                </form>
                @endforeach
                @else
                <h2 class="p-2 text-center">Vous n'avez pas de nouvelles demandes<h2>
                        @endif
            </x-slot>
        </x-dropdown>
        <!-- Settings Dropdown -->

        <x-dropdown class="align-right" width="60">
            <x-slot name="trigger">
                <button class="pb-4">
                    <div class="ml-1 mt-5 mr-12">
                        <svg id="dropdown_svg" width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd">
                            <path d="M12 0c6.623 0 12 5.377 12 12s-5.377 12-12 12-12-5.377-12-12 5.377-12 12-12zm0 1c6.071 0 11 4.929 11 11s-4.929 11-11 11-11-4.929-11-11 4.929-11 11-11zm5.247 8l-5.247 6.44-5.263-6.44-.737.678 6 7.322 6-7.335-.753-.665z" />
                        </svg>
                    </div>
                </button>
            </x-slot>
            <x-slot name="content">
                <!-- Authentication -->
                @auth
                <x-dropdown-link :href="route('profil')" :active="request()->routeIs('profil')">
                    {{ __('MODIFICATION DU PROFIL') }}
                </x-dropdown-link>
                <x-dropdown-link :href="route('posts')" :active="request()->routeIs('posts')">
                    {{ __('MES POSTS') }}
                </x-dropdown-link>
                <form method="POST" action="{{ route('logout') }}">
                    @csrf
                    <x-dropdown-link :href="route('logout')" onclick="event.preventDefault();
                                    this.closest('form').submit();">
                        {{ __('DÉCONNEXION') }}
                    </x-dropdown-link>
                </form>
                @else
                <x-dropdown-link :href="route('login')" :active="request()->routeIs('login')">
                    {{ __('Se connecter') }}
                </x-dropdown-link>
                <x-dropdown-link :href="route('register')" :active="request()->routeIs('register')">
                    {{ __('Inscription') }}
                </x-dropdown-link>
                @endauth
            </x-slot>
        </x-dropdown>
    </div>
    </div>

    <!-- Hamburger -->
    <div class="-mr-2 flex items-center sm:hidden">
        <button @click="open = ! open" class="inline-flex items-center justify-center p-2 rounded-md text-bleu-400 hover:text-bleu-500 hover:bg-bleu-100 focus:outline-none focus:bg-bleu-100 focus:text-bleu-500 transition duration-150 ease-in-out">
            <svg class="h-6 w-6" stroke="currentColor" fill="none" viewBox="0 0 24 24">
                <path :class="{'hidden': open, 'inline-flex': ! open }" class="inline-flex" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h16" />
                <path :class="{'hidden': ! open, 'inline-flex': open }" class="hidden" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
            </svg>
        </button>
    </div>
    </div>
    </div>
    </div>
    <div id="la_ligne"> </div>
    <!-- Responsive Navigation Menu -->
    <div :class="{'block': open, 'hidden': ! open}" class="hidden sm:hidden">
        <div class="pt-2 pb-3 space-y-1">
            <x-responsive-nav-link :href="route('dashboard')" :active="request()->routeIs('dashboard')">
                {{ __('Dashboard') }}
            </x-responsive-nav-link>
        </div>

        <!-- Responsive Settings Options -->
        <div class="pt-4 pb-1 border-t border-bleu-200">
            <div class="px-4">
                @auth
                <div class="font-medium text-base text-bleu-800">{{Auth::user()->firstname}}</div>
                <div class="font-medium text-sm text-bleu-500">{{Auth::user()->email}}</div>
                @endauth
            </div>

            <div class="mt-3 space-y-1">
                <!-- Authentication -->
                @auth
                <form method="POST" action="{{ route('logout') }}">
                    @csrf

                    <x-responsive-nav-link :href="route('logout')" onclick="event.preventDefault();
                                        this.closest('form').submit();">
                        {{ __('Deconnexion') }}
                    </x-responsive-nav-link>
                </form>
                @else
                <form method="POST" action="{{ route('login') }}">
                    @csrf

                    <x-responsive-nav-link :href="route('login')" onclick="event.preventDefault();
                                        this.closest('form').submit();">
                        {{ __('Deconnexion') }}
                    </x-responsive-nav-link>
                </form>
                @endauth

            </div>
        </div>
    </div>
</nav>
