<x-guest-layout>
        <!-- Validation Errors -->
        <x-auth-validation-errors class="mb-4" :errors="$errors" />
        <div class="w-full flex">
        <div class="w-1/2 sm:w-full sm:flex-1 ">
        <h1 class="text-bleu text-center text-5xl font-poppins font-semibold mt-8 mb-8 ml-14">CRÉER UN COMPTE</h1>

        <form method="POST" class="space-y-0.5 max-w-xl ml-20 text-center" action="{{ route('register') }}">
            @csrf

            <!-- username -->
            <div><label for="username" :value="__('username')">Nom d'utilisateur :</label></div>

            <div><x-input id="username" class="mb-4 p-4" type="text" name="username" :value="old('username')" required autofocus /></div>
            
            <!-- Email Address -->
            
            <div><label for="email" :value="__('Email :')">Courriel :</label></div>

            <div><x-input id="email" class="mb-4 p-4" type="email" name="email" :value="old('email')" required /></div>

            <!-- City -->
           
            <div><label for="city" :value="__('Ville')">Ville :</label></div>

            <div><x-input id="city" class="mb-4 p-4" type="text" name="city" :value="old('city')" required /></div>
          

            <!-- Password -->
           
            <div><label for="password" :value="__('Mot de passe :')">Mot de passe :</label></div>

            <div><x-input id="password" class="mb-4 p-4"
                                type="password"
                                name="password"
                                required autocomplete="new-password" /></div>
         

            <!-- Confirm Password -->
          
            <div><label for="password_confirmation" :value="__('Confirmer votre mot de passe')">Confirmation mot de passe :</label></div>

            <div><x-input id="password_confirmation" class="mb-8 p-4"
                                type="password"
                                name="password_confirmation" required /></div>        


                <x-button class="mt-8 content-center">
                    {{ __('S\'enregistrer') }}
                </x-button>
                <p class="text-bleu pt-6 pb-10">Déjà un compte ? <a href="/" class="text-rose hover:text-rosefonce hover:no-underline">Se connecter</a> </p>
        </form>
        </div>
        
    <div class="w-1/2 self-center">
  <img src="./images/index_register.png" alt="rejoignez la famille baristalk" class="max-w-3xl fade-in" />
  </div>
    </div>
</x-guest-layout>
