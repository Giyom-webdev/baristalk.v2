<header class=" body-font bg-beige">
    <x-guest-layout>
    @auth
<!-- <div class="fixed z-10 inset-0 overflow-y-auto" aria-labelledby="modal-title" role="dialog" aria-modal="true">
  <div class="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">

    <div class="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" aria-hidden="true"></div> -->

    <!-- This element is to trick the browser into centering the modal contents. -->
    <span class="hidden sm:inline-block sm:align-middle sm:h-screen" aria-hidden="true">&#8203;</span>
    <div class="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full">
      <div class="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
        <div class="sm:flex sm:items-start">
          <div class="mx-auto flex-shrink-0 flex items-center justify-center h-12 w-12 rounded-full bg-red-100 sm:mx-0 sm:h-10 sm:w-10">
            <!-- Heroicon name: outline/exclamation -->
            <svg class="h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
              <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 9v2m0 4h.01m-6.938 4h13.856c1.54 0 2.502-1.667 1.732-3L13.732 4c-.77-1.333-2.694-1.333-3.464 0L3.34 16c-.77 1.333.192 3 1.732 3z" />
            </svg>
          </div>
          <div class="mt-3 text-center sm:mt-0 sm:ml-4 sm:text-left">
            <h3 class="text-lg leading-6 font-medium text-gray-900" id="modal-title">
              Déconnexion
            </h3>
            <div class="mt-2">
              <p class="text-sm text-gray-500">
                Êtes-vous sûre de vouloir vous déconnecter?
              </p>
            </div>
          </div>
        </div>
      </div>
      <div class="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
      <form method="POST" action="{{ route('logout') }}">
        <button :href="" type="submit" class="w-full inline-flex justify-center rounded-md border border-transparent shadow-sm px-4 py-2 bg-blue-600 text-base font-medium text-white hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-red-500 sm:ml-3 sm:w-auto sm:text-sm">
          Déconnecter
        </button>
    </form>
         <a href="{{ route('dashboard') }}" class="mt-3 w-full inline-flex justify-center rounded-md border border-gray-300 shadow-sm px-4 py-2 bg-white text-base font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 sm:mt-0 sm:ml-3 sm:w-auto sm:text-sm">
          Cancel
        </a>
      </div>
    </div>
  </div>
</div>
@endauth
        <div class="w-full flex">

            <img src="{{asset('/images/index_login.png')}}" alt="c'est l'heure du café social avec baristalk" class="w-1/2 sm:w-screen max-w-3xl mt-8 ml-3">

            <!-- Session Status -->
            <x-auth-session-status class="mb-4" :status="session('status')" />



            <div class="w-1/2 flex-col">
            <h1 class="title-font text-5xl text-center font-poppins font-semibold mt-40 mb-20">CONNECTEZ-VOUS</h1>

                <form method="POST" action="{{ route('login') }}" class="space-y-4 text-center">
                    @csrf
                    <!-- Validation Errors -->
                    <x-auth-validation-errors class="mb-4 " :errors="$errors" />
                    

                    <!-- Email Address -->
                    <div><label for="email" :value="__('Email')">Courriel :</label></div>

                    <div><x-input id="email" type="email" name="email" :value="old('email')" required autofocus /></div>


                    <!-- Password -->
                    <div class="mt-4"><label for="password" :value="__('Password')">Mot de passe :</label></div>

                    <div><x-input id="password" class="mb-10" type="password" name="password" required autocomplete="current-password" /></div>





                    <x-button class="content-center">
                        {{ __('Connexion') }}
                    </x-button>

                    <p class="text-bleu mt-8 pt-8">Pas de Compte ? <a href="/register" class="text-rose hover:text-rosefonce hover:no-underline"> Créer un compte</a> </p>


            </div>
        </div>
        </form>
        

    </x-guest-layout>