<x-app-layout>
  <x-slot name="header">
    <x-titreH1> Mes posts </x-titreH1>
    <div class="mt-16 w-10/12 ml-8 ">
      @foreach ($myPost as $post)
      <div class="border-2 border-bleu ml-12 min-w-full p-3 mb-10 h-36 relative shadow">
        <h1 class="text-rose text-2xl uppercase font-semibold mb-4">username</h1>
        <h2> {{$post->description}}</h1>
          <p class="absolute bottom-0 right-0 p-4 text-bleu text-opacity-60">{{$post->created_at->format('d/m/Y')}}</p>
          @include('inc.delete')
      </div>
      @endforeach
    </div>
  </x-slot>
</x-app-layout>