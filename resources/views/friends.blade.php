<x-app-layout>
        <x-slot name="header"></x-slot>
        <div id='form_animation' class="items-center">
                <div>
                        <img alt="profil" class="rounded-full max-h-60 w-36 mx-auto" src="../images/image_fake.jpg">
                        <div class="mx-6 my-8 text-center items-center">
                                <h1 class="text-3xl w-60  mb-6 mx-auto">{{$friend->username}}</h1>
                                <h2 class="text-rose text-lg mb-8"> Vous êtes amis</h2>
                                        <div class="flex justify-content-center ml-20">
                                                <div class="border-r-2 border-bleu pr-8">
                                                        <div class="my-6"> Courriel</div>
                                                        <div class="mx-6"> Ville</div>
                                                </div>
                                                <div class="pl-8">
                                                        <div class="my-6">{{$friend->email}}</div>
                                                        <div class="mx-6 capitalize">{{$friend-> city}}</div>
                                                </div>
                                        </div>
                                        <div class="flex justify-center my-5 ml-3">
                                        @if( $friend-> score <= 5 ) <img src="../images/debutant.png" alt="Status: debutant" >
                                                @elseif( $friend-> score >= 6 && $friend-> score <= 25) <img src="../images/apprenti.png" alt="Status: apprentis">
                                                        @elseif ( $friend-> score >= 26 && $friend-> score <= 50 ) <img src="../images/connaisseur.png" alt="Status: connaisseur" >
                                                                @elseif ( $friend-> score >= 51)
                                                                <img src="../images/maittre.png" alt="Status: maitre connaisseur" >
                                                                @endif
                                </div>
                                <x-button id="form_action_jquery" name="form_action_jquery" onclick=showForm() class="ml-4">
                                        {{ __('INVITER') }}
                                </x-button>
                        </div>
                </div>

                <div>
                        <form method="POST" action="{{ route('new.meeting') }}" id="form_action_jquery_show" style="display: none; width:1000px; height:500px;" class=" grid border-2 border-bleu p-6 pr-5">
                                @csrf
                                {{ method_field('POST') }}
                                <p class="pb-5" >Voulez-vous planifier une rencontre avec <span class="font-bold text-rose uppercase">{{$friend->username}} </span>?
                                </p>
                                <x-label for="attendee"/>
                                <x-input value="{{$friend->username}}" name="attendee" hidden/>
                                <x-label for="location" :value="__('Lieu de la rencontre')" />
                                <x-input id="location" class="block" type="text" name="meeting_location" required />
                                <x-label for="date" :value="__('Quand ça ?')" />
                                <x-input id="date" class="block " type="datetime-local" name="meeting_date" required />
                                <div class="pl-3">
                                        <x-button name="meeting_id" value="{{$friend->id}}">
                                                {{ __('Inviter') }}
                                        </x-button>
                        </form>
                </div>
        </div>

</x-app-layout>