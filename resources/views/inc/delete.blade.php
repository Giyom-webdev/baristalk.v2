<form action="{{ route('delete_post', [$post->id]) }}" method="POST">
    {{csrf_field()}}
    {{method_field('DELETE')}}
<x-button-retirer type="submit">Retirer</x-button-retirer>
</form>
