<button {{ $attributes->merge(['type' => 'submit', 'class' => 'text-bleu text-lg font-poppins hover:text-blanc bg-beige hover:bg-rose hover:border-rose border-2 border-bleu w-1/3 h-10 ']) }}>
    {{ $slot }}
</button>
