<h1 {{ $attributes->merge(['class' => 'text-2xl text-bleu text-center leading-tight uppercase']) }}>
    {{ $slot }}
</h1>