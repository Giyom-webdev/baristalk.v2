@props(['disabled' => false])

<input {{ $disabled ? 'disabled' : '' }} {!! $attributes->merge(['class' => 'bg-beige border-2 border-bleu w-1/2 h-10']) !!}>
