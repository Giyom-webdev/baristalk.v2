<button {{ $attributes->merge(['type' => 'submit', 'class' => 'text-blanc text-lg font-poppins hover:text-bleu bg-jaune hover:bg-fonce w-1/3 h-10']) }}>
    {{ $slot }}
</button>
