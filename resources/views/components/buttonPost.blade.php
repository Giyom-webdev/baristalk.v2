<button {{ $attributes->merge(['type' => 'submit', 'class' => 'absolute text-bleu text-lg font-poppins hover:text-blanc bg-beige hover:bg-rose hover:border-rose border-2 border-bleu w-1/3 h-10 mt-8 bottom-0 right-0 m-3']) }}>
    {{ $slot }}
</button>