<button {{ $attributes->merge(['type' => 'submit', 'class' => 'focus:outline-none hover:text-bleu text-2xl font-poppins text-blanc bg-rose border-rose border-2 hover:border-bleu py-4 px-12']) }}>
    {{ $slot }}
</button>
