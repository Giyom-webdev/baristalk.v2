<button {{ $attributes->merge(['type' => 'submit', 'class' => 'absolute top-0 right-0 text-blanc text-lg text-center font-poppins hover:text-bleu bg-jaune hover:bg-fonce w-1/5 h-10 m-4']) }}>
    {{ $slot }}
</button>
