@props(['value'])

<label {{ $attributes->merge(['class' => 'block font-medium font-poppinss mb-3 ml-3 text-sm text-bleu']) }}>
    {{ $value ?? $slot }}
</label>
