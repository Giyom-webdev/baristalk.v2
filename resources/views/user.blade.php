<x-app-layout>
    <x-slot name="header">
        <div class="flex justify-evenly mx-auto ">
            <div class="flex">
                <img alt="profil" class="rounded-full max-h-36" src="./images/image_placeholder.jpg">
                <p class="text-4xl self-center ml-10">
                    {{Auth::user()->username}}
                </p>
            </div>
            <x-button class="mr-10 max-w-xs self-center">
                {{ __('Changer la photo de profil') }}
            </x-button>
        </div>
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <x-auth-validation-errors class="mb-4" :errors="$errors" />
            <x-sucess-message class="mb-4" :errors="$errors" />
            <form method="POST" action="{{ route('profile.update') }}">
                @method('PUT')
                @csrf
                <div class="grid grid-cols-2 gap-12 auto-cols-max">
                    <div class="grid grid-rows-2 gap-8 ">
                        <div>
                            <x-label for="username" :value="__('Votre nom de profil')" />
                            <x-input id="username" class="block mt-1 w-full" type="text" name="username" value="{{ auth()->user()->username }}" />
                        </div>
                        <div>
                            <x-label for="email" :value="__('Email')" />
                            <x-input id="email" class="block mt-1 w-full" type="email" name="email" value="{{ auth()->user()->email }}" />
                        </div>
                        <div>
                            <x-label for="city" :value="__('Ville')" />
                            <x-input id="city" class="block mt-1 w-full capitalize" type="text" name="city" value="{{ auth()->user()->city }}" />
                        </div>
                    </div>
                    <div class="grid grid-rows-3 gap-8">
                        <div>
                            <x-label for="new_password" :value="__('Nouveau mot de passe')" />
                            <x-input id="new_password" class="block mt-1 w-full" type="password" name="password" autocomplete="new-password" />
                        </div>
                        <div>
                            <x-label for="confirm_password" :value="__('Confirmation Mot de passe')" />
                            <x-input id="confirm_password" class="block mt-1 w-full" type="password" name="password_confirmation" autocomplete="confirm-password" />
                        </div>
                    </div>
                </div>
                <div class="flex flex-row items-center justify-center mt-5">
                    <x-button class="mr-10">
                        {{ __('Sauvegarder les changements') }}
                    </x-button>
                    <x-button-delete>
                        {{ __('Supprimer le profil') }}
                    </x-button-delete>
                </div>
            </form>
        </div>
    </div>
</x-app-layout>
