<x-app-layout>
    <x-slot name="header">
        <x-titreH1 class="mb-20"> Let's meetup !</x-titreH1>
        <form action="{{ route('add.meeting') }}" class=" border-2 border-bleu p-5" method="POST">
        @csrf
                <x-label for="friendname" :value="__('Avec qui ?')" />
                <x-input id="friendname" class="block mt-1"  type="text" name="username" value="{{$friend->username}}" />
                <x-label for="lieu" :value="__('Lieu de la rencontre')" />
                <x-input id="lieu" class="block mt-1 "  type="text" name="date_meeting" />
                <x-label for="datemeetup" :value="__('Quand ça ?')" />
                <x-input id="datemeetup" class="block mt-1 " type="date" name="location" />
            <div class="flex justify-center">
            <x-button class=" p-3 self-center" >
                {{ __('Créer le meetup') }}
            </x-button>
        </form>
    </x-slot>
</x-app-layout>
