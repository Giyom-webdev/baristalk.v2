<x-app-layout>

    <x-slot name="header"></x-slot>
    <x-sucess-message class="mb-4" :errors="$errors" />
    @if($users -> isNotEmpty())
    @foreach ($users as $user)
    <div class="grid justify-items-stretch place-items-center">
        <form method="POST" action="{{ route('add.friend') }}" class="justify-self-center w-1/2 ml-80" id="addfriend">
            @csrf
            {{ method_field('POST') }}
            <div class="max-w-lg ml-20">
                <img alt="profil" class="rounded-full max-h-60 w-36 m-10" src="./images/image_placeholder.jpg">
                <div>
                    <div class="text-3xl mx-auto mb-10">
                        {{$user->username}}
                    </div>
                </div>
                <div class="flex mx-auto">
                    <div class="border-r-2 border-bleu pr-8">
                        <div class="m-3 ml-0"> Courriel </div>
                        <div class="m-3 ml-0"> Ville</div>
                    </div>
                    <div class="pl-8">
                        <div class="m-4"> {{$user->email}}</div>
                        <div class="m-4"> {{$user->city}}</div>
                    </div>
                </div>
            </div>
            <br>
                @endforeach
                @else
                <h2 class="ml-20 text-lg"> Il n'y a pas d'utilisateur à ce nom là <h2>
                        @endif
            <div>
            <div class="flex justify-center">
                                        @if( $user-> score <= 5 ) <img src="./images/debutant.png" alt="tage debutant" >
                                                @elseif( $user-> score >= 6 && $user-> score <= 25) <img src="../images/apprenti.png" alt="tage apprentis">
                                                        @elseif ( $user-> score >= 26 && $user-> score <= 50 ) <img src="../images/connaisseur.png" alt="tage connaisseur" >
                                                                @elseif ( $user-> score >= 51)
                                                                <img src="../images/maittre.png" alt="tage metre connaisseur" >
                                                                @endif
                                </div>
            <x-button id="received" name="received" value="{{$user->id}}" class="ml-20">
                    {{ __('Devenir Ami') }}
            </x-button>
            </div>
        </form>
    </div>
</x-app-layout>
