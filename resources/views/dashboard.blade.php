<x-app-layout>
  <x-slot name="header">
    <x-sucess-message class="mb-4" :errors="$errors" />
    <div class="flex justify-center mt-6">
      <div class="max-w-lg border-r border-bleu m-14 pr-5 flex flex-col items-center">
        <img alt="profil" class="rounded-full max-h-60 w-36 m-10" src="./images/chef.jpg">
        <div>
          <div class="text-3xl text-center mb-10 uppercase font-extralight ">
            {{Auth::user()->username}}
          </div>
        </div>
          <div class="mt-1 text-center">
            <div class="font-light text-sm uppercase m-3 tracking-widest"> {{Auth::user()->email}}</div>
            <div class="font-light text-sm uppercase m-3 tracking-widest text-center"> {{Auth::user()-> city}}</div>
          </div>
        <div class="flex flex-col m-4 items-center">
            <div class="font-medium text-base uppercase m-6 tracking-widest text-center"> RÉPUTATION</div>
            <div>
        @if( Auth::user()-> score <= 5 )
        <img src="./images/debutant.png" class="" alt="tag debutant">
          @elseif( Auth::user()-> score >= 6 && Auth::user()-> score <= 25)
          <img src="./images/apprenti.png" alt="tag apprentis">
            @elseif ( Auth::user()-> score >= 26 && Auth::user()-> score <= 50 )
             <img  src="./images/connaisseur.png" alt="tag connaisseur">
              @elseif ( Auth::user()-> score >= 51)
              <img src="./images/maittre.png" alt="tag metre connaisseur">
              @endif
              </div>
</div>
        <div class="mt-8 ml-5">
      </div>
      </div>
      <div>
      <div class="mt-20 pt-1 mx-4 ">
        <div class="h-36 bg-beige2 border-2 border-bleu  relative">
          <form action="{{ route('add.post') }}" method="POST" class="bg-beige2">
            {{csrf_field()}}
             <textarea id="borderArea" type="text" name="description" class="form-textarea active:opacity-0 block w-full mt-20 border-none bg-beige2 font-extralight" rows="2" cols="50" placeholder="Écrire un post.." style="margin-top: 10px; resize: none; outline: none; max-width: 800px;" required></textarea>

            @if ($errors->any())
          <div class="w-4/8 m-auto text-center">
          @foreach ($errors->all() as $error )

          <li class=" text-lg list-none text-red-600 font-light">
            {{$error}}
          </li>
          @endforeach

          </div>
          @endif
          <x-buttonPost> {{ __('Envoyer') }}</x-buttonPost>
          </form>
        </div>

      </div>
      <div class="mt-10 mx-4">
      @foreach ($posts as $post)
        @if(isset($post->description))
        <div class="border-2 border-bleu p-3 mb-10 h-36 relative shadow">
            <h1 class="text-rose text-2xl uppercase font-semibold mb-2 tracking-wider">{{$post->username}}</h1>
            <p class="font-extralight"> {{$post->description}}</p>
                <p class="absolute bottom-0 right-0 p-4 text-bleu text-opacity-60 font-light">{{$post->created_at}}</p>
        </div>
        @endif
        @if (isset($post->location))
        <div class=" px-12 py-8 mb-10 h-36 relative shadow" id="background">
          <div class="flex justify-between" >
            <h1 class="tracking-wider text-2xl uppercase font-semibold mb-2">{{$post->organiser}}</h1>
            <p class="text-base">ET</p>
            <h2 class="tracking-wider text-2xl uppercase font-semibold mb-2">{{$post->attendee}}</h2>
            </div>
            <p class="mt-3 text-center text-base"> auront une rencontre au <span class="font-bold uppercase">{{$post->location}}</span> le {{$post->date_meeting}}!</p>
        </div>
        @endif
        @endforeach
    </div>
    </div>
      <div class="ml-8 grid-cols-3">
        <!-- Barre de recherche -->
        <form action="{{ route('search') }}" method="GET" class="ml-12 relative" autocomplete="off">
            <input type="text" class="h-10 pr-8 pl-5 border-2 border-bleu bg-beige2 typeahead shadow font-extralight" name="search" placeholder="Trouver un ami..." required>
              <button type="submit" class="absolute top-4"><i class="fa fa-search text-gray-400 z-20 hover:text-gray-500"></i></button>
        </form>
        <div class=" text-center mt-8 ml-12 border-2 border-bleu bg-beige">
          <h3 class="bg-bleu p-3 text-white tracking-widest font-bold">LISTE D'AMIS</h3>
          <div>
            @if(isset($friendList))
            @foreach ($friendList as $friends)
              <a href="{{ route('friendProfil', $friends->id) }}" class="hover:no-underline">
                <h4 class="text-rose hover:text-rosefonce my-3 capitalize tracking-widest font-medium text-xl" >{{$friends->username}}</h4>
            </a>
            @endforeach
            @endif
          </div>
        </div>

 <div class=" text-center mt-8 ml-12 border-2 border-bleu">
    <h3 class="bg-bleu p-3 text-white shadow tracking-widest font-bold">RENCONTRE</h3>
    <div class="flex flex-col justify-center items-center ">
      @foreach ($myMeeting as $meeting)
      <div class="shadow px-5 py-4 mt-2 mb-4 text-center">
      <h4 class="pb-2 font-extralight text-sm">Rencontre avec :<span class="text-rose uppercase pl-1"><?php
      if (Auth()->user()->username === $meeting->organiser){
      echo $meeting->attendee;
      } if (Auth()->user()->username === $meeting->attendee){
        echo $meeting->organiser;
      };
      ?></span></h4>

      <h5 class="font-extralight text-sm pb-2">Lieu : <span class="uppercase font-bold">{{$meeting->location}}</span> </h5>
      <p class=" text-sm font-extralight pb-2">Date et heure : {{$meeting->date_meeting}} </p>
      <?php
      $tempsMeeting = time() - strtotime($meeting->date_meeting);
      if((time()-(60*60*24)) > strtotime($meeting-> date_meeting))
      {
        echo  '<p style="color:red;" class="text-center text-sm" > expiré </p>';
      }
      if (intval(- $tempsMeeting / 60 / 60  ) < 24 && (- $tempsMeeting / 60 / 60  ) > 0){
        echo '<p style="color:green; " class="text-center text-sm">Votre meeting est dans' . " ";
        echo intval(- $tempsMeeting / 60 / 60  ). " " . "h" . "</p>";
      }
      if (intval(- $tempsMeeting / 60 / 60  ) > 24 ){
      echo '<p style="color:green;" class="text-center text-sm">Votre meeting est dans' . " " ;
        echo intval(- $tempsMeeting / 60 / 60 / 24 ) .  " " . "j" . "</p>";
      }
      ?>
</div>
      @endforeach



</div>
</div>
    </div>
  </x-slot>
  <script>
    var path = "{{ route('search.action')  }}";
    $('input.typeahead').typeahead({
      source: function(query, process) {
        return $.get(path, {
          term: query
        }, function(data) {
          return process(data);
        });
      }
    });
  </script>
</x-app-layout>
