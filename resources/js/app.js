require('./bootstrap');
require('@fortawesome/fontawesome-free/js/all.js');
require('alpinejs');



$('#form_action_jquery').click(function() {
    $('#form_action_jquery_show').toggle("linear", function() {
        $('#form_animation').addClass('grid grid-cols-3 gap-4')
        $('#form_action_jquery_show').addClass('form_action_show')
        $('#form_action_jquery').attr('hidden', 1)
    });
});

/* Jquery document ready , pour le loading du JS plus rapide */
$(window).on("load", function() {
    // weave your magic here.
});