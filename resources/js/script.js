
        var path = "{{ route('autocomplete') }}";
        $('#user_name').typeahead({
            source: function(query, process) {
                return $.get(path, {
                    query: query
                }, function(data) {
                    return process(data);
                });
            }
        });
 