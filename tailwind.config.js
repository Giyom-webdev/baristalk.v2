const defaultTheme = require('tailwindcss/defaultTheme');

module.exports = {
    mode: 'jit',
    purge: [
        './vendor/laravel/framework/src/Illuminate/Pagination/resources/views/*.blade.php',
        './storage/framework/views/*.php',
        './resources/views/**/*.blade.php',
    ],

    theme: {
        flexGrow: {
            '0': 0,
           DEFAULT: 2,
           '1': 1,
        },
        extend: {
            colors: {
                beige: '#EEEBE3',
                bleu: '#002C46',
                rose: '#F78762',
                rosefonce: '#FF4F15',
                jaune: '#FFD470',
                fonce: '#F9C346',
                blanc: '#FFFFFF',
                vert: '#3BA416',
                rouge: '#D63333',
                beige2: '#EEEBD3',
              },
            fontFamily: {
                poppins: 'poppins, sans-serif',
                montserrat: 'montserrat, sans-serif',
            },
            minWidth: {
                '0': '0',
                '1/8': '33%',
                '1/4': '25%',
                '1/2': '50%',
                '3/4': '75%',
                'full': '100%',
            },
            minHeight: {
                '0': '0',
                '1/4': '25%',
                '1/2': '50%',
                '3/4': '75%',
                'full': '100%',
            },
        },
    },

    variants: {
        extend: {
            fontSize: ['hover'],
            fontWeight: ['hover'],
        },
    },

    plugins: [require('@tailwindcss/forms')],
};
