<?php

namespace App\View\Components;

use Illuminate\Support\Facades\Auth;
use Illuminate\View\Component;
use Illuminate\Support\Facades\DB;

use App\Models\Friendship;
use App\Models\User;

class AppLayout extends Component
{
    /**
     * Get the view / contents that represents the component.
     *
     * @return \Illuminate\View\View
     */
    public function render()
    {
        $userId = Auth::id();
        $received = Friendship::where('received', $userId)->where('accepted', 0)->select('sent')->get();
        $receivedId = Friendship::where('received', $userId)->where('accepted', 0)->select('id')->get();
        $friendRequest = User::find($received);
        $refuseRequest = Friendship::find($receivedId);
        $notifications = $received->count();

        /*Calcul des points du user */
        $potsCount = DB::table('Posts')->where('user_id', $userId)->count();
        $potsCount = $potsCount * 10;
        $friendsCount = DB::table('friendships')->where('received', $userId)->orWhere('sent', $userId)->count();
        $friendsCount = $friendsCount * 2;
        $meetupCount = DB::table('meeting')->where('received_meeting', $userId)->orWhere('sent_meeting', $userId)->count();
        $meetupCount = $meetupCount * 25;
        
        $userScore = $potsCount + $friendsCount + $meetupCount;

        $updateScore = User::find($userId)->update(array('score' => $userScore));
        return view('layouts.app', compact('userScore', 'notifications', 'friendRequest'));
    }
}
