<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateProfileRequest;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;

use App\Models\Friendship;
use App\Models\User;
use App\Models\Post;
use App\Models\Meeting;

class UserController extends Controller
{
    public function update(UpdateProfileRequest $request)
    {
        auth()->user()->update($request->only('username', 'email', 'city'));

        if ($request->input('password')) {
            auth()->user()->update([
                'password' => bcrypt($request->input('password'))
            ]);
        }
        return redirect()->route('profil')->with('message', 'Profile sauvegarder avec succes');
    }

    public function index()
    {
        if (Auth::check()) {
            return $this->dashboard();
        } else {
        return view('auth.login');
        }
    }

 
    public function dashboard()
    {
        $userId = Auth::id();
        $username = Auth::user()->username;

        /* Retourne la liste d'amis dans la vue */
        $returnReceivedFriend = Friendship::where('received', $userId)->where('accepted', 1)->select('sent')->get();
        $returnSentFriend = Friendship::where('sent', $userId)->where('accepted', 1)->select('received')->get();
        $friendsReceived = User::find($returnReceivedFriend);
        $friendsSent = User::find($returnSentFriend);
        $friendList = $friendsReceived->merge($friendsSent)->all();

        /* Retourne les posts de l'utilisateur et des amis dans la vue */
        /* Posts de l'utilisateur connecter */
        $myPost = Post::all()->where('user_id', $userId);

        $friendsReceivedPosts = DB::table('friendships')->join('posts', function ($join) {
            $userId = Auth::id();
            $join->on('friendships.received', '=', 'posts.user_id')
                ->where('friendships.sent', '=', $userId)
                ->where('friendships.accepted', '=', 1);
        })->get();
        $friendsSentPosts = DB::table('friendships')->join('posts', function ($join) {
            $userId = Auth::id();
            $join->on('friendships.sent', '=', 'posts.user_id')
                ->where('friendships.received', '=', $userId)
                ->where('friendships.accepted', '=', 1);
        })->get();

        $myMeeting = Meeting::where('sent_meeting', $userId)->orWhere('received_meeting', $userId)->latest()->get();

        $myFriendsMeetingReceived = DB::table('friendships')->join('meeting', function ($join) {
            $userId = Auth::id();
            $join->on('friendships.received', '=', 'sent_meeting')
            ->where('friendships.sent', $userId)
            ->where('friendships.accepted', '=', 1);
        })->get();

        $myFriendsMeetingSent = DB::table('friendships')->join('meeting', function ($join) {
            $userId = Auth::id();
            $join->on('friendships.sent', '=', 'sent_meeting')
            ->where('friendships.received', $userId)
            ->where('friendships.accepted', '=', 1);
        })->get();

        $myFriendsMeeting = $myFriendsMeetingReceived->merge($myFriendsMeetingSent)->all();
        /*Merge tous les posts*/
        $posts = $friendsReceivedPosts->merge($friendsSentPosts)->merge($myPost)->merge($myFriendsMeeting)->sortByDesc('created_at');
        return view('dashboard', compact('friendList', 'posts', 'myMeeting'));
    }
}
