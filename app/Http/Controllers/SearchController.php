<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Friendship;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class SearchController extends Controller
{
    public function search(Request $request){


        $userId = Auth::id();
        // Pour avoir le nom à chercher dans la bar de recherche
        $search = $request->input('search');

        // cherche le nom dans username dans la table users
        $userQuery = User::query()
            ->where('username', 'LIKE', "%{$search}%")
            ->select('id')
            ->get();
        $thisId = $userQuery[0]->id;
        $users = User::find($userQuery);
            if ($thisId  === $userId) { 
                return redirect()->route('profil');
            }else{
                $friendZoneRequest =  Friendship::where('sent', $userId)->orWhere('received', $userId)->value('accepted');              
                return view('search', compact('users', 'friendZoneRequest'));
            }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    function action(Request $request)
    {
            return User::select('username')
                ->where('username', 'like', "%{$request->term}%")
                ->pluck('username');

    }
  
}
