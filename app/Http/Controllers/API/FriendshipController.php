<?php

namespace App\Http\Controllers\api;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

use App\Models\Friendship;
use App\Models\User;
use App\Http\Controllers\Auth\SearchController;

class FriendshipController extends Controller
{

/*     public function friendRequest()
    {
        $userId = Auth::id();
        $received = Friendship::where('received', $userId)->where('accepted', 0)->select('sent')->get();
        $friendRequest = User::find($received);
        return view('friends', compact('friendRequest'));
    } */

    public function acceptRequest(Request $request)
    {
        $userId = Auth::id();
        $sentId = $request->accepted;
        $acceptRequest = Friendship::where('received', $userId)->where('sent', $sentId)->update(['accepted' => 1]);
        return redirect()->route('dashboard')->with('message', 'Vous êtes maintenant ami !');
    }

    public function show($id)
    {
        $friend = User::find($id);
     
        return View('friends', compact('friend'));
    }

    /*
        FUNCTION NEW FRIENDSHIP
        Cette function retourne l'id de l'utilisateur et l'id de la personne a ajouter puis les sauvegarde dans la table friendship

*/
    public function addFriend(Request $request)
    {
        $userId = Auth::id();
        $friendRequest = Friendship::create([
            'sent' => $userId,
            'received' => $request->received
        ]);
        $friendRequest->save();
        return redirect()->route('dashboard')->with('message', "Vous avez ajouté un utilisateur avec succès !");
    }
    public function viewUser()
    {

        return view('users.friend-profile');
    }


    public function refuseRequest(Request $request)
    {
        $userId = Auth::id();
        $refused = $request->refuse;
        $deleteFriend = Friendship::where('received', $userId)->where('sent', $refused)->delete();
        return redirect()->route('dashboard')->with('message', "Vous un ami en moins");
    }
}
