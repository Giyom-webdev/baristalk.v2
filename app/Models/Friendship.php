<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Friendship extends Model
{
    use HasFactory;
    protected $table = 'friendships';
    
    protected $fillable = [
        'id',
        'sent',
        'received',
        'accepted',
    ];
    public function users()
    {
        return $this->belongsToMany(User::class);
    }

}
