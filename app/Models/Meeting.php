<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class meeting extends Model
{
    use HasFactory;
    protected $table = 'meeting';

    protected $fillable = [
        'id',
        'sent_meeting',
        'received_meeting',
        'organiser',
        'attendee',
        'date_meeting',
        'location',
    ];

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

}
