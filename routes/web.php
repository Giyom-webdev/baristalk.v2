<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\Api\PostController;
use App\Http\Controllers\Api\MeetingController;
use App\Http\Controllers\Api\FriendshipController;
use App\Http\Controllers\SearchController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [UserController::class, 'index']);
Route::get('/login', [UserController::class, 'index']);

Route::view('login', 'login')->name('login');

Route::group(['middleware' => 'auth'], function() {

    Route::view('user', 'user')->name('profil');

    /* UserController  */
     Route::get('dashboard',[UserController::class, 'dashboard'])->name('dashboard');
     Route::get('index',[UserController::class, 'dashboard'])->name('index');
     Route::put('profile', [UserController::class, 'update'])->name('profile.update');



    /* PostController  */
     Route::get('posts',[PostController::class, 'posts'])->name('posts');
     Route::delete('/posts/{id}',[PostController::class, 'destroy'])->name('delete_post');
     Route::post('add.post',[PostController::class, 'store'])->name('add.post');

   /* FriendshipController  */
     /* Route::get('/', [FriendshipController::class, 'friendRequest'])->name('friends'); */
     Route::post('add.friend', [FriendshipController::class, 'addFriend'])->name('add.friend');
     Route::put('accept.friend', [FriendshipController::class, 'acceptRequest'])->name('accept.friend');
     Route::post('refuse.friend', [FriendshipController::class, 'refuseRequest'])->name('refuse.friend');

    /*searchController, route pour la recherche*/
     Route::get('search', [SearchController::class, 'search'])->name('search');

    /*amiController, route pour le profil d'un ami*/
     Route::get('friend/{id}', [FriendshipController::class, 'show'])->name('friendProfil');

    /*Route pour l'autocompile*/
     Route::get('search/autocomplete', [SearchController::class, 'action'])->name('search.action');

   /* MeetingController  */

     Route::get('meeting',[MeetingController::class, 'show'])->name('meeting');
     Route::post('new.meeting',[MeetingController::class, 'store'])->name('new.meeting');
});

require __DIR__.'/auth.php';
